﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class inputName : MonoBehaviour {

    public InputField playerfield;

    static string name;

	// Use this for initialization
	void Start () {

        name = PlayerPrefs.GetString("PlayerName");

        playerfield.text = name;

	}
	
	// Update is called once per frame
	public void StartPressed () {
        name = playerfield.text;

        PlayerPrefs.SetString("PlayerName", name);
	}
}
