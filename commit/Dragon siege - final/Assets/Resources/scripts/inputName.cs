﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class inputName : MonoBehaviour {

    public InputField playerfield;

    static string name;

	// Use this for initialization
	void Start () {
        //name is uqual toPlayerName
        name = PlayerPrefs.GetString("PlayerName");

        playerfield.text = name;

	}
	
	// Update is called once per frame
	public void StartPressed () {

        // set name to playerfield.text
        name = playerfield.text;

        //PlayerName is equal to name
        PlayerPrefs.SetString("PlayerName", name);
	}
}
