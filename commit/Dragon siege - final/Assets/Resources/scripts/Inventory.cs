﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Inventory : MonoBehaviour
{
    bool checkit;

 

    public Sprite[] mItems = new Sprite[5];
    //creatse a sprite list

    changing_name changenamescript;
    //calls changing name script


    public void Start()
    {
        //sets the values of the elements in the list
        //set boolean checkit to false


        
        
        mItems[0] = Resources.Load<Sprite>("sprites/rocks");
        
        
        mItems[1] = Resources.Load<Sprite>("sprites/stick");
        
        
        mItems[2] = Resources.Load<Sprite>("sprites/flint");
        
        
        mItems[3] = Resources.Load<Sprite>("sprites/key");
        
        
        mItems[4] = Resources.Load<Sprite>("sprites/sword");

        checkit = false;

 

    }

    public void Update()
    {
        //calls updateInventory 
        updateInventory();
    }


    public void updateInventory()
    {
        //brings the flowchar from changename script
        //creates bools for every item you can collect
        changenamescript = GameObject.Find("changeName").GetComponent<changing_name>();
        bool rocks = changenamescript.flowchart.GetBooleanVariable("rocks");
        bool flint = changenamescript.flowchart.GetBooleanVariable("flint");
        bool stick = changenamescript.flowchart.GetBooleanVariable("stick");
        bool sword = changenamescript.flowchart.GetBooleanVariable("sword");
        bool key = changenamescript.flowchart.GetBooleanVariable("key");

        //for testing purposes
        Debug.Log("Rocks: " + rocks);
        Debug.Log("Flint: " + flint);
        Debug.Log("Stick: " + stick);
        Debug.Log("Sword: " + sword);
        Debug.Log("Key: " + key);
        
        //if the boolean is not equal to check it (which is false)
        if (rocks != checkit)
        {
            checkit = rocks;

           // print("my bool has changed to: " + rocks);
            if (rocks == true)
            {

                //creates pickedItem
                //sets ItemImage1 to pickedItem
                //pickedItem is equal to pickedSprite(which contians the item image)
                Image pickedItem;
                pickedItem = GameObject.Find("ItemImage1").GetComponent<Image>();
                Sprite pickedSprite = mItems[0];
                pickedItem.sprite = pickedSprite;
                checkit = false;
            }
        }

        if (flint != checkit)
        {
            checkit = flint;

         //   print("my bool has changed to: " + rocks);
            if (flint == true)
            {

                //creates pickedItem
                //sets ItemImage2 to pickedItem
                //pickedItem is equal to pickedSprite(which contians the item image)
                Image pickedItem;
                pickedItem = GameObject.Find("ItemImage2").GetComponent<Image>();
                Sprite pickedSprite = mItems[2];
                pickedItem.sprite = pickedSprite;
                checkit = false;
            }
        }

        if (stick != checkit)
        {
            checkit = stick;

         //   print("my bool has changed to: " + rocks);
            if (stick == true)
            {

                //creates pickedItem
                //sets ItemImage3 to pickedItem
                //pickedItem is equal to pickedSprite(which contians the item image)
                Image pickedItem;
                pickedItem = GameObject.Find("ItemImage3").GetComponent<Image>();
                Sprite pickedSprite = mItems[1];
                pickedItem.sprite = pickedSprite;
                checkit = false;
            }
        }

        if (key != checkit)
        {
            checkit = key;

            //print("my bool has changed to: " + rocks);
            if (key == true)
            {
                //creates pickedItem
                //sets ItemImage4 to pickedItem
                //pickedItem is equal to pickedSprite(which contians the item image)
                Image pickedItem;
                pickedItem = GameObject.Find("ItemImage4").GetComponent<Image>();
                Sprite pickedSprite = mItems[3];
                pickedItem.sprite = pickedSprite;
                checkit = false;
            }
        }

        if (sword != checkit)
        {
            sword = rocks;

           // print("my bool has changed to: " + rocks);
            if (sword == true)
            {

                //creates pickedItem
                //sets ItemImage5 to pickedItem
                //pickedItem is equal to pickedSprite(which contians the item image)
                Image pickedItem;
                pickedItem = GameObject.Find("ItemImage5").GetComponent<Image>();
                Sprite pickedSprite = mItems[4];
                pickedItem.sprite = pickedSprite;
                checkit = false;
            }
        }





    }



   
}
    
