﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Fungus;

public class changing_name : MonoBehaviour
{

    public Flowchart flowchart;
    string getPlayerName;


    

    // Use this for initialization
    void Start()
    {
        //calls function getText
        getText();

    }

    public void getText()
    {
        //setting getPlayerName to PlayerName(from prefabs)
        getPlayerName = PlayerPrefs.GetString("PlayerName");
        //setting the variable playerName to getPlayerName
        flowchart.SetStringVariable("playerName", getPlayerName);
        

    }
}